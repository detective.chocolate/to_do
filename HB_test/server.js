const express = require("express");
const expressHbs = require("express-handlebars");
const hbs = require("hbs");
const app = express();

app.set("view engine", "hbs");
app.set("view options", {layout: "layouts/layout"});

hbs.registerHelper("getTime", function(){
     
    var myDate = new Date();
    var hour = myDate.getHours();
    var minute = myDate.getMinutes();
    var second = myDate.getSeconds();
    if (minute < 10) {
        minute = "0" + minute;
    }
    if (second < 10) {
        second = "0" + second;
    }
    return "Текущее время: " + hour + ":" + minute + ":" + second;;
});

 


hbs.registerPartials(__dirname + "/views/partials");


app.use("/contact", function(request, response){
     
    response.render("contact.hbs", {
        title: "Мои контакты",
        emailsVisible: true,
        emails: ["gavgav@mycorp.com", "rek@kek.com"],
        phone: "+1234567890"
    });
});



app.use("/", function(request, response){
     
    response.render("home.hbs", )
});
app.listen(3000);